package br.unifor.jme.rms;

public class Priority {

	/** c�digo da prioridade: 0 - alta, 1 - baixa. */
	private int code;
	
	/** Nome da prioridade */
	private String name;
	
	/** Indica prioridade alta */
	public static final String HIGH = "alta";
	
	/** Indica prioridade baixa */
	public static final String LOW = "baixa";
	
	/** Construtor da classe Priority
	 * @param name - HIGH ou LOW */
	public Priority(String name) {
		
		if(name.equals(Priority.HIGH)){
			this.code = 0;
			this.name = Priority.HIGH;
		}else{
			this.code = 1;
			this.name = Priority.LOW;
		}
	}
	
	/** Construtor da classe Priority
	 * @param code - c�digo da prioridade */
	public Priority(int code) {
		
		if(code == 0){
			this.code = code;
			this.name = Priority.HIGH;
		}else{
			this.code = 1;
			this.name = Priority.LOW;
		}
	}
	
	/** Verifica se a prioridade � ALTA */
	public boolean isPriorityHigh(){
		return HIGH.equals(name);
	}

	/** Verifica se a prioridade � BAIXA */
	public boolean isPriorityLow(){
		return LOW.equals(name);
	}
	
	public int getCode() {
		return code;
	}

	public String getName() {
		return name;
	}

	public String toString() {
		return "Priority [code=" + code + ", name=" + name + "]";
	}
}
