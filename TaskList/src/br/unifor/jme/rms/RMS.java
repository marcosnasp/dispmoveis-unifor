package br.unifor.jme.rms;

import java.util.Vector;

import javax.microedition.rms.RecordComparator;
import javax.microedition.rms.RecordEnumeration;
import javax.microedition.rms.RecordFilter;
import javax.microedition.rms.RecordStore;
import javax.microedition.rms.RecordStoreException;

/** Classe que realiza opera��es de persist�ncia no RMS */
public class RMS {

	private static IRMSListener listener;

	public static void setListener(IRMSListener l) {
		listener = l;
	}

	/**
	 * Persiste a tarefa no RMS.
	 * 
	 * @param task - Tarefa a ser persistida.
	 * @return recordId - Id do registro da tarefa no RMS.
	 * @throws RecordStoreException - Exce��o lan�ada quando h� erro na persist�ncia.
	 */
	public int saveTask(Task task) throws RecordStoreException {
		RecordStore rs = RecordStore.openRecordStore("rs_tasks", true);

		try {
			byte[] data = task.toByteArray();
			int recordId = task.getRecordId();

			/* recordId = -1 indica registro novo */
			if (recordId == -1) {
				recordId = rs.addRecord(data, 0, data.length);
				task.setRecordId(recordId);

				if (listener != null) {
					listener.registroPersistido();
				}
			} else {
				rs.setRecord(recordId, data, 0, data.length);
			}

			return recordId;
		} finally {
			rs.closeRecordStore();
		}
	}

	/**
	 * Exclui a tarefa no RMS.
	 * 
	 * @param task - Tarefa a ser exclu�da.
	 * @return recordId - Id do registro da tarefa no RMS.
	 * @throws RecordStoreException - Exce��o lan�ada quando h� erro na exclus�o.
	 */
	public int deleteTask(Task task) throws RecordStoreException {
		RecordStore rs = RecordStore.openRecordStore("rs_tasks", true);

		try {
			int recordId = task.getRecordId();

			if (recordId != -1) {
				rs.deleteRecord(recordId);
			}

			return recordId;
		} finally {
			rs.closeRecordStore();
		}
	}

	/** Busca tarefas no RMS.
	 * @param filter - RecordFilter
	 * @param comp - RecordComparator
	 * @return vector - array de tarefas obtidas na busca.
	 * @throws RecordStoreException - Exce��o lan�ada quando h� erro na busca.
	 *  */
	public Vector searchTasks(RecordFilter filter, RecordComparator comp)
			throws RecordStoreException {
		RecordStore rs = RecordStore.openRecordStore("rs_tasks", true);

		Vector tasks = new Vector(rs.getNumRecords());
		
		int recId = -1;
		byte[] record = null;
		Task task = null;

		try {
			RecordEnumeration recEnum = rs
					.enumerateRecords(filter, comp, false);

			while (recEnum.hasNextElement()) {
				recId = recEnum.nextRecordId();
				record = rs.getRecord(recId);

				task = new Task(recId, record);
				tasks.addElement(task);
			}
		} finally {
			rs.closeRecordStore();
		}

		return tasks;
	}
}
