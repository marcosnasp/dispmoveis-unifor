package br.unifor.jme.sorter;

import javax.microedition.rms.RecordComparator;

import br.unifor.jme.rms.Task;

/** Comparador de datas de atividades. */
public class TaskSorterByData implements RecordComparator {

	public int compare(byte[] rec1, byte[] rec2) {

		Task task_1 = new Task(-1, rec1);
		Task task_2 = new Task(-1, rec2);

		if (task_1.getDateHour() == task_2.getDateHour()) {
			return EQUIVALENT;
		} else if (task_1.getDateHour() > task_2.getDateHour()) {
			return FOLLOWS;
		} else {
			return PRECEDES;
		}
	}
}