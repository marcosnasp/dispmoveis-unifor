package br.unifor.jme.midlet;

import javax.microedition.lcdui.Display;
import javax.microedition.midlet.MIDlet;
import javax.microedition.midlet.MIDletStateChangeException;

import br.unifor.jme.ui.FormTaskList;

public class TaskListMIDlet extends MIDlet {

	private FormTaskList fmTaskList;
			 
	public TaskListMIDlet() {
		
	}

	protected void startApp() throws MIDletStateChangeException {
		
		if(fmTaskList == null){
			
			fmTaskList = new FormTaskList(this);
		}
		
		Display display = Display.getDisplay(this);
		display.setCurrent(fmTaskList);
	}
	
	protected void destroyApp(boolean arg0) throws MIDletStateChangeException {
		
	}

	protected void pauseApp() {
		
	}
}