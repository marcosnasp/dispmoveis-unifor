package br.unifor.jme.ui;

import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.Displayable;
import javax.microedition.lcdui.Form;

import br.unifor.jme.midlet.TaskListMIDlet;

public class FormViewDetailsTasks extends Form implements CommandListener {

	/** Referência a MIDlet da aplicação. */
	private TaskListMIDlet midlet;
	
	/** Referência para tela da lista de tarefas */
	private Displayable previousScreen;
	
	/** Command para retornar para a tela da lista de atividades. */
	private Command cmdBack;
	
	public FormViewDetailsTasks(TaskListMIDlet midlet, Displayable previousScreen) {
		super("Detalhes das tarefas");
		
		this.midlet = midlet;
		this.previousScreen = previousScreen;
		
	}

	public void commandAction(Command arg0, Displayable arg1) {
		
	}


}
