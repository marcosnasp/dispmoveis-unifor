package br.unifor.jme.ui;

import java.util.Vector;

import javax.microedition.lcdui.ChoiceGroup;
import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.Display;
import javax.microedition.lcdui.Displayable;
import javax.microedition.lcdui.Form;
import javax.microedition.rms.RecordStoreException;

import br.unifor.jme.midlet.TaskListMIDlet;
import br.unifor.jme.rms.IRMSListener;
import br.unifor.jme.rms.RMS;
import br.unifor.jme.rms.Task;
import br.unifor.jme.sorter.TaskSorterByData;

public class FormTaskList extends Form implements CommandListener, IRMSListener{

	/** Check boxes da lista de tarefas */
	private ChoiceGroup cgTasks;
	
	/** Command para inserir uma nova tarefa. */
	private Command cmdInsertTask;
	
	/** Command para mostrar detalhes de uma tarefa */
	private Command cmdDetailTask;
	
	/** Editar uma tarefa */
	private Command cmdUpdateTask;
	
	/** Command para excluir uma tarefa */
	private Command cmdDeleteTask;
	
	/** Command para registrar a realização de uma tarefa */
	private Command cmdDoTask;
	
	/** Command para indicar as preferências da aplicação:
	 * ordenação, filtro etc */
	private Command cmdPreferences;
	
	/** Command para sair da aplicação */
	private Command cmdExit;
	
	/** Referência para TaskListMIDlet */
	private TaskListMIDlet midlet;
	
	/** Vector que armazena a lista de tarefas  */
	private Vector tasks;
	
	/** Construtor de Choice group do tipo check boxes.
	 * @param midlet - Referência para TaskListMIDlet */
	public FormTaskList(TaskListMIDlet midlet) {
		
		super("Lista de Tarefas");
				
		this.midlet = midlet;
		
		this.cgTasks = new ChoiceGroup("", ChoiceGroup.MULTIPLE);
		this.append(cgTasks);
		
		RMS.setListener(this);
		this.loadTasks();
		
		this.cmdDoTask = new Command("Marcar realizadas", Command.OK, 0);
		this.cmdInsertTask = new Command("Criar", Command.SCREEN, 1);
		this.cmdDetailTask = new Command("Detalhar", Command.SCREEN, 1);
		this.cmdUpdateTask = new Command("Editar", Command.SCREEN, 2);
		this.cmdDeleteTask = new Command("Excluir", Command.SCREEN, 3);
		this.cmdPreferences = new Command("Preferências", Command.SCREEN, 3);
		this.cmdExit = new Command("Sair", Command.EXIT, 3);
		
		this.addCommand(cmdDoTask);
		this.addCommand(cmdInsertTask);
		this.addCommand(cmdDetailTask);
		this.addCommand(cmdUpdateTask);
		this.addCommand(cmdDeleteTask);
		this.addCommand(cmdPreferences);
		this.addCommand(cmdExit);
	
		this.setCommandListener(this);
	}
		
	/** Carrega lista de tarefas, buscanco no RMS e atribuindo no check box. */
	public void loadTasks() {

		cgTasks.deleteAll(); // remove todas tarefas do choice group

		RMS bd = new RMS();

		try {
			tasks = bd.searchTasks(null, new TaskSorterByData());
		} catch (RecordStoreException ex) {
			ex.printStackTrace();
		}

		int qtd = tasks.size();
		for (int i = 0; i < qtd; i++) {
			cgTasks.append(((Task)tasks.elementAt(i)).getDescription(), null);
		}
	}

	/** Trata eventos de commands. 
	 * @param c - Command que originou o evento. 
	 * @param d - Displayable onde o evento foi disparado. */
	public void commandAction(Command c, Displayable d) {
		
		if(c == cmdDoTask){
			//TODO implementar
			
		} else if(c == cmdInsertTask){
			
			Display display = Display.getDisplay(midlet);
			display.setCurrent(new FormAddTask(midlet, this));
			
		} else if(c == cmdDetailTask){

			Display display = Display.getDisplay(midlet);
			display.setCurrent(new FormViewDetailsTasks(midlet, this));
			
		} else if(c == cmdUpdateTask){
			//TODO implementar
		} else if(c == cmdDeleteTask){
			//TODO implementar
		} else if(c == cmdPreferences){
			//TODO implementar
		} else if(c == cmdExit){
			 midlet.notifyDestroyed();
		}
	}

	/** Chama método para carregar as tarefas. */
	public void registroPersistido() {
		this.loadTasks();
	}
}
