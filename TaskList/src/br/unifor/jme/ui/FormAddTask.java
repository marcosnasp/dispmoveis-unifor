package br.unifor.jme.ui;

import java.util.Date;

import javax.microedition.lcdui.Alert;
import javax.microedition.lcdui.AlertType;
import javax.microedition.lcdui.ChoiceGroup;
import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.DateField;
import javax.microedition.lcdui.Display;
import javax.microedition.lcdui.Displayable;
import javax.microedition.lcdui.Form;
import javax.microedition.lcdui.TextField;
import javax.microedition.rms.RecordStoreException;

import br.unifor.jme.midlet.TaskListMIDlet;
import br.unifor.jme.rms.Category;
import br.unifor.jme.rms.Priority;
import br.unifor.jme.rms.RMS;
import br.unifor.jme.rms.Task;

public class FormAddTask extends Form implements CommandListener {

	/** TextField para descri��o da atividade. */
	private TextField tfDescription;

	/** ChoiceGroup para escolha da categoria da atividade. */
	private ChoiceGroup cgCategory;

	/** ChoiceGroup para escolha da prioridade da atividade */
	private ChoiceGroup cgPriority;

	/** DateField para data em que a atividade deve ser realizada. */
	private DateField dfDate;

	/** Command para salvar a atividade criada. */
	private Command cmdSave;

	/** Command para retornar para a tela da lista de atividades. */
	private Command cmdBack;

	/** Refer�ncia a MIDlet da aplica��o. */
	private TaskListMIDlet midlet;
	
	/** Refer�ncia para tela da lista de tarefas */
	private Displayable previousScreen;
	
	/** Timeout de 2,5 s */
	private static final int TIMEOUT = 2500;

	/** Construtor de FormAddTask.TaskListMIDlet midlet, Displayable previousScreen
	 * @param midlet - refer�ncia para TaskListMIDlet */
	public FormAddTask(TaskListMIDlet midlet, Displayable previousScreen) {

		super("Nova Tarefa");

		this.midlet = midlet;
		this.previousScreen = previousScreen;

		tfDescription = new TextField("Descri��o:", "", 50, TextField.ANY);

		dfDate = new DateField("Data/Hora:", DateField.DATE_TIME);
		dfDate.setDate(new Date());

		// colocar usando set (objetos no estado consistente)
		setCgCategory(createCgCategoryGroup());
		setCgPriority(createCgPriorityGroup());
		
		append(tfDescription);
		append(getCgCategory());
		append(getCgPriority());
		append(dfDate);

		cmdSave = new Command("Salvar", Command.OK, 0);
		cmdBack = new Command("Voltar", Command.BACK, 1);

		addCommand(cmdSave);
		addCommand(cmdBack);

		setCommandListener(this);
	}

	public void saveTask() {
		
		//cria objeto para a insercao no RMS
		Task task = createTask();

		RMS bd = new RMS();
		try {
			if(task != null) {
				bd.saveTask(task);
				showAlert("Tarefa cadastrada!", AlertType.INFO, previousScreen);
			} else {
				showAlert("Campos da tarefa invalidos!", AlertType.ERROR, previousScreen);
			}
		} catch (RecordStoreException ex) {
			showAlert("Erro ao cadastrar tarefa!", AlertType.ERROR, this);
		}
	}

	/**
	 * Cria uma tarefa para que a mesma seja inserida no RMS
	 * retorna a Task caso todos os dados obrigatorios estejam preenchidos
	 * retorna null caso a Task tenha algum valor invalido.
	 * @return Task
	 */
	private Task createTask() {
		
		Task task = new Task();
		
		//obt�m descri��o
		if(tfDescription.getString().trim().length() > 0){
			task.setDescription(tfDescription.getString().trim());
		}else{
			showAlert("Informe a descri��o da atividade!", AlertType.WARNING, this);
			return null;
		}
		
		//obt�m categoria
		Category c = new Category(getCgCategory().getSelectedIndex());
		task.setCategory(c.getCode());
		
		//obt�m prioridade
		Priority p = new Priority(getCgPriority().getSelectedIndex());
		task.setPriority(p.getCode());
		
		//obt�m data e hora
		if(dfDate.getDate() != null){
			task.setDateHour(dfDate.getDate().getTime());
		}else{
			showAlert("Informe data e a hora da atividade!", AlertType.WARNING, this);
			return null;
		}
		return task;
	}

	/** Trata eventos de commands. 
	 * @param c - Command que originou o evento. 
	 * @param d - Displayable onde o evento foi disparado. */
	public void commandAction(Command c, Displayable d) {
		if(c == cmdSave){
			saveTask();
		}else if(c == cmdBack){
			Display display = Display.getDisplay(midlet);
            display.setCurrent(previousScreen);
		}
	}
	
	/** Dispara alerta na tela.
	 * @param msg - Mensagem a ser exibida no alerta.
	 * @param tipo - Tipo do alerta.
	 * */
	public void showAlert(String msg, AlertType tipo, Displayable nextScreen) {
        Alert alert = new Alert("Alerta", msg, null, tipo);
        alert.setTimeout(TIMEOUT);
       
        Display.getDisplay(midlet).setCurrent(alert, nextScreen);
    }

	public void setCgPriority(ChoiceGroup cgPriorityGroup) {
		if(cgPriorityGroup != null) {
			this.cgPriority = cgPriorityGroup;
		}
	}
	
	public ChoiceGroup getCgPriority() {
		return cgPriority;
	}
	
	public void setCgCategory(ChoiceGroup cgCategoryGroup) {
		if(cgCategoryGroup != null) {
			cgCategory = cgCategoryGroup;
		}
	}
	
	public ChoiceGroup getCgCategory() {
		return cgCategory;
	}
	
	
	/** 
	 * Criacao das Listas de Prioridades para as tarefas
	 * @return ChoiceGroup
	 */
	private ChoiceGroup createCgPriorityGroup() {
		String[] prioridades = { Priority.HIGH, Priority.LOW };
		return new ChoiceGroup("Prioridade:", ChoiceGroup.EXCLUSIVE,
				prioridades, null);
	}

	/** 
	 * Criacao das Listas de Categorias para as tarefas
	 * @return ChoiceGroup
	 */
	private ChoiceGroup createCgCategoryGroup() {
		String[] categorias = { Category.WORK, Category.APPOINTMENT,
				Category.LEISURE, Category.OTHER };
		return new ChoiceGroup("Categoria:", ChoiceGroup.EXCLUSIVE,
				categorias, null);
	}

}
