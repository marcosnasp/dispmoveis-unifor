package br.unifor.jme.net;

import java.io.IOException;

import javax.microedition.io.Connector;
import javax.wireless.messaging.MessageConnection;
import javax.wireless.messaging.TextMessage;

import br.unifor.jme.model.Task;

/**
 * Classe respons�vel pelo envio de uma tarefa por SMS
 * */
public class SMSSender extends Thread {
	
	private Task task;
	private String to;
	
	public void run() {
		MessageConnection conn = null;
		try {
			conn = (MessageConnection) Connector.open("sms://" + to);
			TextMessage msg = (TextMessage) conn.newMessage(MessageConnection.TEXT_MESSAGE );
			msg.setPayloadText( task.toString());
			conn.send(msg);
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if ( conn != null ) {
				try {
					conn.close();
				} catch (IOException e) {
					e.printStackTrace();
				}		
			}
		}		
	}

	/**
	 * Envia uma tarefa via SMS
	 * @param task Tarefa a ser enviada
	 * @param to N�mero do celular de destino do SMS
	 */
	public void send( Task tsk, String dest){
		task = tsk;
		to = dest;
		start();
	}
}