package br.unifor.jme.filter;

import javax.microedition.rms.RecordFilter;

import br.unifor.jme.model.Priority;
import br.unifor.jme.model.Task;

/** Filtro por prioridade */
public class FilterByPriority implements RecordFilter {

	private Priority priority;
	
	/** Construtor
	 * @param priority - tipo de prioridade usada na tarefa */
	public FilterByPriority(Priority priority) {
		this.priority = priority;
	}

	public boolean matches(byte[] data) {
		
		Task task = new Task(-1, data);
		Priority priority = new Priority(task.getPriority());
		
		return this.priority.equals(priority);
	}

}
