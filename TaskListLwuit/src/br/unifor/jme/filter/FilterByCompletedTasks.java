package br.unifor.jme.filter;

import javax.microedition.rms.RecordFilter;

import br.unifor.jme.model.Task;

/** Filtro de tarefas realizadas */
public class FilterByCompletedTasks implements RecordFilter {

	private boolean taskStatus;
	
	public FilterByCompletedTasks() {
		this.taskStatus = true;
	}
	
	public boolean matches(byte[] data) {
		
		Task task = new Task(-1, data);
		
		if(taskStatus == task.isCompleted()) {
			return true;
		} else {
			return false;
		}
		
	}

}
