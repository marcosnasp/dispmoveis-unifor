package br.unifor.jme.filter;

import javax.microedition.rms.RecordFilter;

import br.unifor.jme.model.Category;
import br.unifor.jme.model.Task;

/** Filtro por categoria */
public class FilterByCategory implements RecordFilter {

	private Category category;
	
	/** Construtor
	 * @param category - tipo de categoria utilizada no filtro */
	public FilterByCategory(Category category) {
		this.category = category;
	}
	
	public boolean matches(byte[] data) {
	
		Task task = new Task(-1, data);
		Category cat = new Category(task.getCategory());
	
		return category.equals(cat);
	}

}
