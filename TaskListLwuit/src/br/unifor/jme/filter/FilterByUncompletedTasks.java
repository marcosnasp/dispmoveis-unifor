package br.unifor.jme.filter;

import javax.microedition.rms.RecordFilter;

import br.unifor.jme.model.Task;

/** Filtro por tarefas n�o realizadas */
public class FilterByUncompletedTasks implements RecordFilter {

	private boolean taskStatus;

	/** Construtor */
	public FilterByUncompletedTasks() {
		this.taskStatus = false;
	}
	
	public boolean matches(byte[] data) {

		Task task = new Task(-1, data);
		
		if(taskStatus == task.isCompleted()) {
			return true;
		} else {
			return false;
		}
		
	}

}
