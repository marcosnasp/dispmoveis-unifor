package br.unifor.jme.model;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public class Task {

	/** Descri��o da tarefa */
	private String description;
	
	/** Descri��o da categoria */
	private int category;
	
	/** Descri��o da prioridade */
	private int priority;
	
	/** Data e hora da tarefa */
	private long dateHour;
	
	/** Status da Tarefa se concluida true caso contrario false */
	private boolean isCompleted;
	
	/** id do registro do record store do RMS */
	private int recordId = -1;
	
	/** Construtor sem argumentos */
	public Task() {
	
	}

	/** Construtor que recebe os atributos como argumento
	 * @param descrition - descri��o da tarefa
	 * @param category - categoria da tarefa
	 * @param priority - prioridade da tarefa
	 * @param date - data e hora da tarefa */
	public Task(String description, int category, int priority,
			long date, boolean isCompleted) {
		super();
		this.description = description;
		this.category = category;
		this.priority = priority;
		this.dateHour = date;
		this.isCompleted = isCompleted;
	}
	
	/** Construtor que recebe o id do registro e um array de bytes com os 
	 * atributos da tarefa. */
	public Task(int recordId, byte[] data){
		
		ByteArrayInputStream bais = new ByteArrayInputStream(data);
        DataInputStream dis = new DataInputStream(bais);
        
        try {
		   	description = dis.readUTF(); //descri��o
		   	category = dis.readInt(); //c�digo da categoria
		   	priority = dis.readInt(); //c�digo da prioridade
		   	dateHour = dis.readLong(); //long da data e hora
		   	isCompleted = dis.readBoolean(); // boolean com status da tarefa
        } catch (IOException e) {
					
		}
        
        this.recordId = recordId; 
	}
	
	/** Obt�m um array de bytes a partir dos atributos da tarefa. */
	public byte[] toByteArray() {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        DataOutputStream dos = new DataOutputStream(baos);

        try {
            dos.writeUTF(description);
            dos.writeInt(category);
            dos.writeInt(priority);
            dos.writeLong(dateHour);
            dos.writeBoolean(isCompleted);
        } catch (IOException ex) { }

        return baos.toByteArray();
    }
	
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getCategory() {
		return category;
	}

	public void setCategory(int category) {
		this.category = category;
	}

	public int getPriority() {
		return priority;
	}

	public void setPriority(int priority) {
		this.priority = priority;
	}

	public long getDateHour() {
		return dateHour;
	}

	public void setDateHour(long dateHour) {
		this.dateHour = dateHour;
	}

	public int getRecordId() {
		return recordId;
	}

	public void setRecordId(int recordId) {
		this.recordId = recordId;
	}

	public void setCompleted(boolean isCompleted) {
		this.isCompleted = isCompleted;
	}

	public boolean isCompleted() {
		return isCompleted;
	}
	
	public String toString() {
		return "Task [descrition=" + description + ", category=" + category
				+ ", priority=" + priority + ", dateHour=" + dateHour + "]";
	}

	
}
