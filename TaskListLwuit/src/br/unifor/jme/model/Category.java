package br.unifor.jme.model;

public class Category {
	
	/** c�digo da categoria */
	private int code;
	
	/** nome da categoria */
	private String name;
	
	/** Indica categoria trabalho */
	public static final String WORK = "work";
	
	/** Indica categoria lazer */
	public static final String LEISURE = "leisure";
	
	/** Indica categoria compromisso, encontro ou hora marcada */
	public static final String APPOINTMENT = "appointment";
	
	/** Indica categoria gen�rica, diferente das demais */
	public static final String OTHER = "other";
	
	/** Construtor da classe Category
	 * @param name - nome da categoria */
	public Category(String name) {
		
		if(name.equals(WORK)){
			this.code = 0;
			this.name = WORK;
		}else if(name.equals(LEISURE)){
			this.code = 1;
			this.name = LEISURE;
		}else if(name.equals(APPOINTMENT)){
			this.code = 2;
			this.name = APPOINTMENT;
		}else{
			this.code = 3;
			this.name = OTHER;
		}
	}
	
	/** Construtor da classe Category
	 * @param code - c�digo da categoria */
	public Category(int code) {
		
		if(code == 0){
			this.code = code;
			this.name = WORK;
		}else if(code == 1){
			this.code = code;
			this.name = LEISURE;
		}else if(code == 2){
			this.code = code;
			this.name = APPOINTMENT;
		}else{
			this.code = 3;
			this.name = OTHER;
		}
	}
	
	/** Verifica se a categoria � "trabalho" */
	public boolean isWork(){
		return WORK.equals(name);
	}

	/** Verifica se a categoria � "lazer" */
	public boolean isLeisure(){
		return LEISURE.equals(name);
	}
	
	/** Verifica se a categoria � "compromisso" */
	public boolean isAppointment(){
		return APPOINTMENT.equals(name);
	}
	
	/** Verifica se a categoria � "outra" */
	public boolean isOther(){
		return OTHER.equals(name);
	}
	
	public int getCode() {
		return code;
	}

	public String getName() {
		return name;
	}

	public String toString() {
		return "Category [code=" + code + ", name=" + name + "]";
	}
	
	public boolean equals(Object obj) {
		
		Category category = (Category) obj;
		
		if(this.getCode() == category.getCode()) { 
			return true;
		} else {
			return false;
		}
		
	}
}
