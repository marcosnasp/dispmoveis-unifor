package br.unifor.jme.sorter;

import javax.microedition.rms.RecordComparator;

import br.unifor.jme.model.Task;

/** Comparador de datas por ordem decrescente de atividades. */
public class TaskSorterByDataDesc implements RecordComparator {

	public int compare(byte[] rec1, byte[] rec2) {

		Task task_1 = new Task(-1, rec1);
		Task task_2 = new Task(-1, rec2);

		if (task_1.getDateHour() == task_2.getDateHour()) {
			return EQUIVALENT;
		} else if (task_1.getDateHour() > task_2.getDateHour()) {
			return PRECEDES;
		} else {
			return FOLLOWS;
		}
	}
}