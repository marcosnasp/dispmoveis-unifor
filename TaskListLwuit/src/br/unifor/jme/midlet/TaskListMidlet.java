package br.unifor.jme.midlet;

import java.io.IOException;

import javax.microedition.midlet.MIDlet;

import br.unifor.jme.rms.PreferencesSearch;
import br.unifor.jme.rms.TaskDAO;
import br.unifor.jme.rms.TaskDAOImpl;
import br.unifor.jme.sorter.TaskSorterByDataAsc;
import br.unifor.jme.ui.FormTaskList;
import br.unifor.jme.util.Alarm;

import com.sun.lwuit.Display;
import com.sun.lwuit.Form;
import com.sun.lwuit.plaf.UIManager;
import com.sun.lwuit.util.Resources;

public class TaskListMidlet extends MIDlet {
	
	/** Form corrente da aplica��o */
	private Form currentForm;
	
	private TaskDAO taskDAO = new TaskDAOImpl();
	
	public TaskListMidlet() { 
		
	}

	public void startApp() {
		
		// init the LWUIT Display
		Display.init(this);
		try {
			Resources theme = Resources.open("/TaskListTheme.res");//tema da app
			UIManager.getInstance().setThemeProps(theme.getTheme(theme.getThemeResourceNames()[0]));

			// Verifica se existe alguma tarefa no hor�rio para alertar
			Alarm.checkAlarm( taskDAO.searchTasks(null, new TaskSorterByDataAsc()));
		} catch (IOException e) {
			
			e.printStackTrace();
		}
		
		if(getCurrentForm() == null) {
			setCurrentForm(new FormTaskList(this, new PreferencesSearch()));
		}
	}

	public void pauseApp() {
	}

	public void destroyApp(boolean unconditional) {
		// Agenda pr�xima tarefa na lista 
		Alarm.registerNext( taskDAO.searchTasks(null, new TaskSorterByDataAsc()));
	}
	
	/** Seta Form corrente */
	public void setCurrentForm(Form currentForm) {
		this.currentForm = currentForm;
	}

	/** Obt�m inst�ncia de Form corrente. */
	public Form getCurrentForm() {
		return currentForm;
	}
}