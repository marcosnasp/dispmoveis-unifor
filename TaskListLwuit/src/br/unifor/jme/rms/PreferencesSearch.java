package br.unifor.jme.rms;

import javax.microedition.rms.RecordComparator;
import javax.microedition.rms.RecordFilter;

import br.unifor.jme.filter.FilterByUncompletedTasks;

/** Classe que encapsula comportamento referente �s prefer�ncias de
 * filtro e ordena��o das tarefas */
public class PreferencesSearch {

	private RecordFilter filter;
	
	private RecordComparator comparator;
	
	public PreferencesSearch(RecordFilter filter, RecordComparator comparator) {
		this.filter = filter;
		this.comparator = comparator;
	}

	public PreferencesSearch() {
		this.filter = new FilterByUncompletedTasks();
		this.comparator = null;
	}
	
	/**
	 * Altera a exibi��o das tarefas.
	 * De acordo com as prefer�ncias do usuario
	 * @param filter
	 */
	public void setFilter(RecordFilter filter) {
		this.filter = filter;
	}

	public RecordFilter getFilter() {
		return filter;
	}

	/**
	 * Altera a ordena��o das tarefas
	 * Utilizado para realizar ordena��es nas tarefas
	 * Ordena��o em ordem ASC e DESC.
	 * @param comparator
	 */
	public void setComparator(RecordComparator comparator) {
		this.comparator = comparator;
	}

	public RecordComparator getComparator() {
		return comparator;
	}
	
	
}
