package br.unifor.jme.rms;

import java.util.Vector;

import javax.microedition.rms.RecordComparator;
import javax.microedition.rms.RecordFilter;
import javax.microedition.rms.RecordStoreException;

import br.unifor.jme.model.Task;


public class TaskDAOImpl implements TaskDAO {
	
	private RMS database;
	
	public TaskDAOImpl() {
		database = new RMS();
	}
	
	/* (non-Javadoc)
	 * @see br.unifor.jme.rms.TaskDAO#searchTasks(javax.microedition.rms.RecordFilter, javax.microedition.rms.RecordComparator)
	 */
	public Vector searchTasks(RecordFilter filter,
			RecordComparator recordComparator) {
		
		Vector tasks = new Vector();
		
		try {
			tasks = database.searchTasks(filter, recordComparator);
			return tasks;
		} catch (RecordStoreException e) {
			
			e.printStackTrace();
		}
		return tasks;	
	}
	
	/* (non-Javadoc)
	 * @see br.unifor.jme.rms.TaskDAO#deleteTask(br.unifor.jme.model.Task)
	 */
	public void deleteTask(Task task) {
		try {
			if(task != null) {
				database.deleteTask(task);
			}
		} catch (RecordStoreException e) {
			e.printStackTrace();
		}
		
	}
	
	/* (non-Javadoc)
	 * @see br.unifor.jme.rms.TaskDAO#searchOneTaskForDescription(java.lang.String)
	 */
	public Task searchOneTaskForDescription(String description) {
		
		try {
			return database.searchOneTaskForDescription(description);
		} catch (RecordStoreException e) {
			e.printStackTrace();
		}
		return null;
	}
	

	/* (non-Javadoc)
	 * @see br.unifor.jme.rms.TaskDAO#saveTask(br.unifor.jme.model.Task)
	 */
	public String saveTask(Task task) {
		try {
			if(task != null) {
				database.saveTask(task);
				return null;
			} else {
				return null;
			}
		} catch (RecordStoreException ex) {
			ex.printStackTrace();
			return null;
		}
	}
}