package br.unifor.jme.rms;

import java.util.Vector;

import javax.microedition.rms.RecordComparator;
import javax.microedition.rms.RecordFilter;

import br.unifor.jme.model.Task;


/** Interface que define as opera��es realizadas sobre a lista de tarefas. */
public interface TaskDAO {

	/** Pesquisa tarefas no RMS de acordo com par�metros filtro e comparador de registros. *
	 * @param filter - Filtro de registros.
	 * @param recordComparator - Comparador de registros.
	 * @return Vector com as tarefas obtidas na pesquisa. */
	Vector searchTasks(RecordFilter filter, RecordComparator recordComparator);
	
	/** Salva uma nova tarefa.
	 * @param task - Nova tarefa a ser salva. */
	String saveTask(Task task);
	
	/** Exclui uma tarefa do RMS.
	 * @param task - Tarefa a ser exclu�da. */
	void deleteTask(Task task);
	
	/** Busca a tarefa que tem determinada descri��o.
	 * @param description - Descri��o da tarefa a ser pesquisada.
	 * @return Task - Tarefa encontrada ou <code>null</code> 
	 * caso a tarefa pesquisada n�o exista. */
	Task searchOneTaskForDescription(String description);
}
