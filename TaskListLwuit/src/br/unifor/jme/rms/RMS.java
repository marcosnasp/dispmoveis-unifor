package br.unifor.jme.rms;

import java.util.Vector;

import javax.microedition.rms.RecordComparator;
import javax.microedition.rms.RecordEnumeration;
import javax.microedition.rms.RecordFilter;
import javax.microedition.rms.RecordStore;
import javax.microedition.rms.RecordStoreException;

import br.unifor.jme.model.Task;

/** Classe que realiza opera��es de persist�ncia no RMS */
public class RMS {

	private static IRMSListener listener;
	
	private static final String RMS_WITH_TASKS_CONCLUDED = "rs_tasks_concluded";
	
	private static final String RMS_WITH_TASKS_NOT_CONCLUDED = "rs_tasks";

	private String rms;
	
	public static void setListener(IRMSListener l) {
		listener = l;
	}

	public RMS(String rms) {
		this.setRms(rms);
	}
	
	public RMS() {
		this.setRms(RMS_WITH_TASKS_NOT_CONCLUDED);
	}
	
	/**
	 * Persiste a tarefa no RMS.
	 * 
	 * @param task - Tarefa a ser persistida.
	 * @return recordId - Id do registro da tarefa salva no RMS.
	 * @throws RecordStoreException Exce��o lan�ada quando h� erro na persist�ncia.
	 */
	public int saveTask(Task task) throws RecordStoreException {
		RecordStore rs = RecordStore.openRecordStore(getRms(), true);

		try {
			byte[] data = task.toByteArray();
			int recordId = task.getRecordId();

			/* recordId = -1 indica registro novo */
			if (recordId == -1) {
				recordId = rs.addRecord(data, 0, data.length);
				task.setRecordId(recordId);

				if (listener != null) {
					listener.registroPersistido();
				}
			} else {
				rs.setRecord(recordId, data, 0, data.length);
			}

			return recordId;
		} finally {
			rs.closeRecordStore();
		}
	}

	/**
	 * Exclui a tarefa no RMS.
	 * 
	 * @param task - Tarefa a ser exclu�da.
	 * @return recordId - Id do registro da tarefa no RMS.
	 * @throws RecordStoreException - Exce��o lan�ada quando h� erro na exclus�o.
	 */
	public int deleteTask(Task task) throws RecordStoreException {
		RecordStore rs = RecordStore.openRecordStore(getRms(), true);

		try {
			int recordId = task.getRecordId();

			if (recordId != -1) {
				rs.deleteRecord(recordId);
			}
			return recordId;
		
		} finally {
			rs.closeRecordStore();
		}
	}

	/** Busca tarefas no RMS.
	 * @param filter - RecordFilter.
	 * @param comp - RecordComparator.
	 * @return vector - array de tarefas obtidas na busca.
	 * @throws RecordStoreException Exce��o lan�ada quando h� erro na busca.
	 *  */
	public Vector searchTasks(RecordFilter filter, RecordComparator comp)
			throws RecordStoreException {
		RecordStore rs = RecordStore.openRecordStore(getRms(), true);

		Vector tasks = new Vector(rs.getNumRecords());
		
		int recId = -1;
		byte[] record = null;
		Task task = null;
		
		try {
			RecordEnumeration recEnum = rs.enumerateRecords(filter, comp, false);

			while (recEnum.hasNextElement()) {
				recId = recEnum.nextRecordId();
				record = rs.getRecord(recId);

				task = new Task(recId, record);
				tasks.addElement(task);
			}
		} finally {
			rs.closeRecordStore();
		}
		
		return tasks;
	}
	
	/**
	 * Busca a tarefa que tem determinada descri��o.
	 * @param description - Descri��o da tarefa a ser pesquisada.
	 * @return A tarefa encontrada na busca, caso exista, ou null 
	 * quando nenhuma tarefa for encontrada.
	 * @throws RecordStoreException
	 */
	public Task searchOneTaskForDescription(String description) throws RecordStoreException {
		RecordStore rs = RecordStore.openRecordStore(getRms(), true);
		Task task = null;
		
		//obt�m todas as tarefas existente no RMS.
		Vector tasks = searchTasks(null, null);
		
		try {
			if(description != null && description.trim().length() > 0) {
				for (int i = 0; i < tasks.size(); i++) {
					task = ((Task) tasks.elementAt(i));
					if(task.getDescription().toLowerCase().equals(description.toLowerCase())) {
						return task;
					}
				}
			}
			return task;
			
		} finally {
			rs.closeRecordStore();
		}
	}

	/**
	 * Permite a mudan�a de rms para trabalhar com o RMS de tarefas
	 * concluidas e com o de n�o concluidas. 
	 * Para isso passe a String com o RMS.
	 * @param rms
	 */
	public void setRms(String rms) {
		if(rms.equals(RMS_WITH_TASKS_CONCLUDED)) { 
			this.rms = RMS_WITH_TASKS_CONCLUDED;
		} else {
			this.rms = RMS_WITH_TASKS_NOT_CONCLUDED;
		}
		
	}

	public String getRms() {
		return rms;
	}

}
