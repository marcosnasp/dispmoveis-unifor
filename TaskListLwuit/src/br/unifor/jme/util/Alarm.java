package br.unifor.jme.util;

import java.util.Date;
import java.util.Vector;

import javax.microedition.io.ConnectionNotFoundException;
import javax.microedition.io.PushRegistry;

import com.sun.lwuit.Dialog;

import br.unifor.jme.model.Task;

public class Alarm {
	
	private static final int MINUTE = 60000; //1000*60
	
	/**
	 * Registra uma tarefa para alarmar em uma determinada data/hora
	 * @param task Tarefa que deve ser agendada 
	 * */
	private static void register( Task task){
		try {
			PushRegistry.registerAlarm("br.unifor.jme.midlet.TaskListMidlet", task.getDateHour());
			//System.out.println("Registrando task: " + task );
		} catch (ConnectionNotFoundException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Verifica se existe alguma tarefa agendada para o hor�rio em que a aplica��o 
	 * foi executada. Caso exista, exibe um alerta informado da Tarefa e agenda a pr�xima tarefa
	 * via Alarm.register() 
	 * @param tasks Tarefas a serem verificadas segundo a data
	 * */
	public static void checkAlarm(Vector tasks){
		long now = (new Date()).getTime();
		Task task = null;

		// Toler�ncia em minutos a ser considerada na compara��o entre a data em que a aplica��o
		// iniciou e a data da tarefa em quest�o
		long tolerance = 2 * MINUTE;
		
		for( int i = 0; i < tasks.size(); ++i ){
			task = (Task) tasks.elementAt(i);
			// Verifica se estar compreendida no intervalo
			if ( task.getDateHour() >= (now - tolerance) && 
				 task.getDateHour() <= (now + tolerance) ){
				
				Dialog.show("Alarm", "Task '" + task.getDescription() + "' is scheduled to start in a few minutes.", "OK", "");
				
			}
		} 
	}
	
	/**
	 * Pega a pr�xima tarefa na lista ( ordenada por data asc) e agenda via Alarm.register() 
	 * @param tasks Tarefas a serem verificadas segundo a data
	 * */	
	public static void registerNext( Vector tasks){
		long now = (new Date()).getTime();
		Task task = null;
		
		long tolerance = 2000;
		for( int i = 0; i < tasks.size(); ++i ){
			task = (Task) tasks.elementAt(i);
			if ( task.getDateHour() > (now + tolerance) ) {
				register(task);
				break;
			}
		}
		
	}
}
