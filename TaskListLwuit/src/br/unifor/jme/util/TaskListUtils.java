package br.unifor.jme.util;

import java.util.Calendar;
import java.util.Date;

public class TaskListUtils {

	/** Converte data no formato long para String formatada.
	 * @param dateTime - Data no formato long.
	 * @return String formatada seguindo o padr�o MM/dd/YYYY. */
	public static String getDataFormatada(long dateTime){
		
		Date data = new Date(dateTime);
		Calendar calendarDate = Calendar.getInstance();
		calendarDate.setTime(data);
		int mes = calendarDate.get(Calendar.MONTH);
		int dia = calendarDate.get(Calendar.DAY_OF_MONTH);
		int ano = calendarDate.get(Calendar.YEAR);
		
		String separador = "/";
		
		StringBuffer sb = new StringBuffer();
		sb.append(mes).append(separador).append(dia).append(separador).append(ano);
		
		return sb.toString();
	}
}
