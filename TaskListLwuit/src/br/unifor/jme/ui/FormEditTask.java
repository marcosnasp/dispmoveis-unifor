package br.unifor.jme.ui;

import java.util.Date;

import br.unifor.jme.midlet.TaskListMidlet;
import br.unifor.jme.model.Category;
import br.unifor.jme.model.Priority;
import br.unifor.jme.model.Task;
import br.unifor.jme.rms.TaskDAOImpl;

import com.sun.lwuit.ButtonGroup;
import com.sun.lwuit.Calendar;
import com.sun.lwuit.Command;
import com.sun.lwuit.Dialog;
import com.sun.lwuit.Form;
import com.sun.lwuit.Label;
import com.sun.lwuit.RadioButton;
import com.sun.lwuit.TextField;
import com.sun.lwuit.animations.CommonTransitions;
import com.sun.lwuit.animations.Transition;
import com.sun.lwuit.events.ActionEvent;
import com.sun.lwuit.events.ActionListener;
import com.sun.lwuit.layouts.BoxLayout;

public class FormEditTask extends Form implements ActionListener {
	
	/** label descri��o */
	private Label labelDescription;
	
	/** textfield descri��o */
	private TextField textDescription;
	
	/** array de radio buttons de prioridade */
	private RadioButton[] radioPriority;
	
	/** array de radio buttons de categoria */
	private RadioButton[] radioCategory;
 	
	/** grupo de radio buttons de prioridade */
	private ButtonGroup buttonGroupPriority;
	
	/** grupo de radio buttons de categoria */
	private ButtonGroup buttonGroupCategory;
	
	/** label prioridade */
	private Label labelPriority;
	
	/** label categoria */
	private Label labelCategory;
	
	/** Command voltar */
	private Command back;
	
	/** Command salvar */
	private Command save;
	
	/** componente calend�rio */
	private Calendar calendar;
	
	/** DAO de tarefas */
	private TaskDAOImpl taskDAO;
	
	/** Refer�ncia para TaskListMIDlet */
	private TaskListMidlet midlet;
	
	/** Tarefa a ser atualizada */
	private Task taskToEdit;
	
	/** Dura��o da transi��o entre telas */
	private static final int DURATION = 500;

	/** horizontal slide left to right */
	private Transition in = CommonTransitions.createSlide(
			CommonTransitions.SLIDE_HORIZONTAL, true, DURATION);

	/** horizontal slide right to left */
	private Transition out = CommonTransitions.createSlide(
			CommonTransitions.SLIDE_HORIZONTAL, true, DURATION);
	
	/** Mensagem para campo obrigat�rio descri��o  */
	private static final String DESCRIPTION_IS_REQUIRED = 
		"You must type a description to new task!";
	
	/** Mensagem para cria��o com sucesso de atividade */
	private static final String SUCESSO = 
		"Task was update with success!";
	
	/**
	 * Construtor de FormEditTask
	 * @param midlet - Refer�ncia para TaskListMIDlet
	 */
	public FormEditTask(TaskListMidlet midlet, Task taskToEdit) {
	
		super("Edit Task");
		setLayout(new BoxLayout(BoxLayout.Y_AXIS));
		 
		this.midlet = midlet;
		
		this.taskToEdit = taskToEdit;
		
		// Cria o label e a descricao da tarefa para o Form de Add Task
		labelDescription = new Label("Task Description");
		textDescription = new TextField(40);
		
		// adiciona os componentes ao Form
		addComponent(labelDescription);
		addComponent(textDescription);
		
		// cria lista com objetos RadioButton[] para prioridades e categorias
		radioPriority = createRadioButtonPriorityGroup();
		radioCategory = createRadioButtonCategoryGroup();
				
		labelCategory = new Label("Choose a category");
		addComponent(labelCategory);
		buttonGroupCategory = addListRadioButtonCategoryToForm();
		
		labelPriority = new Label("Choose a priority");
		addComponent(labelPriority);
		buttonGroupPriority = addListRadioButtonPriorityToForm();
		
		calendar = new Calendar();
		addComponent(calendar);
				
		//preenche os campos para a edicao da task
		textDescription.setText(taskToEdit.getDescription());
		buttonGroupCategory.setSelected(taskToEdit.getCategory());
		buttonGroupPriority.setSelected(taskToEdit.getPriority());
		calendar.setSelectedDate(new Date(taskToEdit.getDateHour()));

		save = new Command("Save", 0);
		addCommand(save);
		
		back = new Command("Back", 1);
		addCommand(back);
		
		addCommandListener(this);
		
		taskDAO = new TaskDAOImpl();
		
		setTransitionOutAnimator(in);
		setTransitionOutAnimator(out);
		
		show();
	}
	
	/**
	 * Adiciona os objetos RadioButton[] Categories para o Form
	 * Adiciona Eventos aos objetos e agrupa os radio buttons em ButtonGroups.
	 * @return ButtonGroup
	 */
	private ButtonGroup addListRadioButtonCategoryToForm() {
		
		buttonGroupCategory = new ButtonGroup();
		for (int tam = 0; tam < radioCategory.length; tam++) {
			addComponent(radioCategory[tam]);
			buttonGroupCategory.add(radioCategory[tam]);
		}
		return buttonGroupCategory;
	}
	
	/**
	 * Adiciona os objetos RadioButton[] priorities para o Form
	 * Adiciona Eventos aos objetos e agrupa os radio buttons em ButtonGroups.
	 * @return ButtonGroup
	 * @param priorities
	 * @param f
	 */
	private ButtonGroup addListRadioButtonPriorityToForm() {
		
		buttonGroupPriority = new ButtonGroup();
		for (int tam = 0; tam < radioPriority.length; tam++) {
			addComponent(radioPriority[tam]);
			buttonGroupPriority.add(radioPriority[tam]);
		}
		
		return buttonGroupPriority;
	}
	
	/** 
	 * Criacao das Listas de Prioridades para as tarefas
	 * @return CheckBox
	 */
	private RadioButton[] createRadioButtonPriorityGroup() {
		RadioButton[] radioButtonPriority = {new RadioButton(Priority.HIGH), new RadioButton(Priority.LOW)};
		
		return radioButtonPriority;
	}

	/** 
	 * Criacao das Listas de Categorias para as tarefas
	 * @return ChoiceGroup
	 */
	private RadioButton[] createRadioButtonCategoryGroup() {
		RadioButton[] radioButtonCategory = {
				new RadioButton(Category.WORK), new RadioButton(Category.LEISURE),
				new RadioButton(Category.APPOINTMENT), new RadioButton(Category.OTHER)};
		
		return radioButtonCategory;
	}

	/**
	 * Captura os conteudos do FormAddTask e constroe um objeto Task com os mesmos
	 * @return Task
	 */
	public Task captureTaskForm() {
		
		Task task = new Task();
		
		// Para a Edicao -- evita inserir um registro quando na verdade deveria editar
		task.setRecordId(taskToEdit.getRecordId());
		
		String description = textDescription.getText().trim();
		Category category = new Category(buttonGroupCategory.getSelectedIndex());
		Priority priority = new Priority(buttonGroupPriority.getSelectedIndex());
		Date date = calendar.getDate();
		
		if(description.length() > 0) {
			task.setDescription(description);
		} else {
			Dialog.show("", DESCRIPTION_IS_REQUIRED, "OK", "");
			return null;
		}
		
		task.setCategory(category.getCode());
		task.setPriority(priority.getCode());
		task.setDateHour(date.getTime());
			
		return task;
	}
	
	/** Tratamento de eventos de commands. */
	public void actionPerformed(ActionEvent ev) {
		
		Command cmd = ev.getCommand();
		
		if(cmd == null) return;
		
		switch(cmd.getId()){
		
			case 0:
				saveTask();
				break;
				
			case 1:
				back();
		}
	}
	
	/** Salva a nova tarefa */
	private void saveTask(){
		
		Task task = captureTaskForm();
		if(task != null) {
			taskDAO.saveTask(task);
			Dialog.show("", SUCESSO, "OK", "");
			back();
		}
	}
	
	/** Retorna para a tela de lista de tarefas */
	private void back(){
		
		this.midlet.setCurrentForm(new FormTaskList(this.midlet));
	}
}
