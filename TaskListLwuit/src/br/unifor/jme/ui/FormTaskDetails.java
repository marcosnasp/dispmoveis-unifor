package br.unifor.jme.ui;

import br.unifor.jme.midlet.TaskListMidlet;
import br.unifor.jme.model.Category;
import br.unifor.jme.model.Priority;
import br.unifor.jme.model.Task;
import br.unifor.jme.rms.IRMSListener;
import br.unifor.jme.rms.RMS;
import br.unifor.jme.util.TaskListUtils;

import com.sun.lwuit.Command;
import com.sun.lwuit.Form;
import com.sun.lwuit.Label;
import com.sun.lwuit.TextField;
import com.sun.lwuit.animations.CommonTransitions;
import com.sun.lwuit.animations.Transition;
import com.sun.lwuit.events.ActionEvent;
import com.sun.lwuit.events.ActionListener;
import com.sun.lwuit.layouts.BoxLayout;

public class FormTaskDetails extends Form implements ActionListener, IRMSListener {

	/** command para voltar para a lista de tarefas */
	private Command cmdBack;
	
	/** textfield com a categoria da tarefa */
	private TextField tfCategoria;
	
	/** textfield com a descri��o da tarefa */
	private TextField tfDescricao;
	
	/** textfield com a prioridade da tarefa */
	private TextField tfPrioridade;
	
	/** textfield com a data da tarefa */
	private TextField tfData;
	
	/** textfield que indica se a tarefa est� conclu�da. */
	private TextField tfConcluded;
	
	/** label para a descri��o da tarefa */
	private Label labelDescription;
	
	/** label para a prioridade da tarefa */
	private Label labelPriority;
	
	/** label para a categoria da tarefa */
	private Label labelCategory;
	
	/** label para a data da tarefa */
	private Label labelData;
	
	/** label que indica se a tarefa est� conclu�da */
	private Label labelConcluido;
	
	/** Refer�ncia para TaskListMidlet */
	private TaskListMidlet midlet;
	
	/** Dura��o da transi��o entre telas */
	private static final int DURATION = 500;

	/** horizontal slide left to right */
	private Transition in = CommonTransitions.createSlide(
			CommonTransitions.SLIDE_HORIZONTAL, true, DURATION);

	/** horizontal slide right to left */
	private Transition out = CommonTransitions.createSlide(
			CommonTransitions.SLIDE_HORIZONTAL, true, DURATION);
	
	/** Construtor de form com descri��o de uma tarefa
	 * @param midlet - Refer�ncia para TaskListMIDlet */
	public FormTaskDetails(TaskListMidlet midlet, Task task) {
		
		super("Details of a task");
		setLayout(new BoxLayout(BoxLayout.Y_AXIS));
		
		this.midlet = midlet;
		
		RMS.setListener(this);
		
		Priority priority = new Priority(task.getPriority());
		Category category = new Category(task.getCategory());
				
		labelDescription = new Label("Description:");
		labelCategory = new Label("Category");
		labelPriority = new Label("Priority");
		labelData = new Label("Date");
		labelConcluido = new Label("Task is done");
		
		tfDescricao = new TextField(40);
		tfDescricao.setText(task.getDescription());
		tfDescricao.setEditable(false);
		
		tfCategoria = new TextField(15);
		tfCategoria.setText(category.getName());
		tfCategoria.setEditable(false);
				
		tfPrioridade = new TextField(5);
		tfPrioridade.setText(priority.getName());
		tfPrioridade.setEditable(false);
		
		tfData = new TextField(10);
		tfData.setText(TaskListUtils.getDataFormatada(task
							.getDateHour()));
		tfData.setEditable(false);
		
		tfConcluded = new TextField(5);
		tfConcluded.setText(task.isCompleted() ? "Yes" : "No");
		tfConcluded.setEditable(false);
		
		cmdBack = new Command("Back", 0);
		
		//add componentes no form
		addComponent(labelDescription);
		addComponent(tfDescricao);
		
		addComponent(labelCategory);
		addComponent(tfCategoria);
		
		addComponent(labelPriority);
		addComponent(tfPrioridade);
		
		addComponent(labelData);
		addComponent(tfData);
		
		addComponent(labelConcluido);
		addComponent(tfConcluded);
		
		addCommand(cmdBack);
			
		addCommandListener(this);
		
		setTransitionOutAnimator(in);
		setTransitionOutAnimator(out);
		
		show();
	}
	
	/** Chama m�todo para carregar as tarefas. */
	public void registroPersistido() {
		
	}

	/** Tratamento de eventos de commands. */
	public void actionPerformed(ActionEvent ev) {

		Command cmd = ev.getCommand();
		
		if(cmd == null) return;
		
		switch(cmd.getId()){
		
			case 0:
				midlet.setCurrentForm(new FormTaskList(midlet));
		}
	}
}
