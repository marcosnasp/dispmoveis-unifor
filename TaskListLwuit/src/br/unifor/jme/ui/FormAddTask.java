package br.unifor.jme.ui;

import java.util.Date;

import br.unifor.jme.midlet.TaskListMidlet;
import br.unifor.jme.model.Category;
import br.unifor.jme.model.Priority;
import br.unifor.jme.model.Task;
import br.unifor.jme.rms.PreferencesSearch;
import br.unifor.jme.rms.TaskDAOImpl;

import com.sun.lwuit.ButtonGroup;
import com.sun.lwuit.Calendar;
import com.sun.lwuit.Command;
import com.sun.lwuit.Dialog;
import com.sun.lwuit.Form;
import com.sun.lwuit.Label;
import com.sun.lwuit.RadioButton;
import com.sun.lwuit.TextField;
import com.sun.lwuit.animations.CommonTransitions;
import com.sun.lwuit.animations.Transition;
import com.sun.lwuit.events.ActionEvent;
import com.sun.lwuit.events.ActionListener;
import com.sun.lwuit.geom.Dimension;
import com.sun.lwuit.layouts.BoxLayout;

/** Form com componentes para criar uma nova tarefa */
public class FormAddTask extends Form implements ActionListener {
	
	/** label descri��o */
	private Label labelDescription;
	
	/** textfield descri��o */
	private TextField textDescription;
	
	/** textfield hour */
	private TextField textHour;

	/** textfield minutes */
	private TextField textMinute;
	
	/** descri��o hora */
	private Label labelHour;
	
	/** array de radio buttons de prioridade */
	private RadioButton[] radioPriority;
	
	/** array de radio buttons de categoria */
	private RadioButton[] radioCategory;
 	
	/** grupo de radio buttons de prioridade */
	private ButtonGroup buttonGroupPriority;
	
	/** grupo de radio buttons de categoria */
	private ButtonGroup buttonGroupCategory;
	
	/** label prioridade */
	private Label labelPriority;
	
	/** label categoria */
	private Label labelCategory;

	/** Command voltar */
	private Command back;
	
	/** Command salvar */
	private Command save;
	
	/** componente calend�rio */
	private Calendar calendar;
	
	/** DAO de tarefas */
	private TaskDAOImpl taskDAO;
	
	/** Refer�ncia para TaskListMIDlet */
	private TaskListMidlet midlet;
	
	/** Dura��o da transi��o entre telas */
	private static final int DURATION = 500;

	/** horizontal slide left to right */
	private Transition in = CommonTransitions.createSlide(
			CommonTransitions.SLIDE_HORIZONTAL, true, DURATION);

	/** horizontal slide right to left */
	private Transition out = CommonTransitions.createSlide(
			CommonTransitions.SLIDE_HORIZONTAL, true, DURATION);
	
	/** Mensagem para campo obrigat�rio descri��o  */
	private static final String DESCRIPTION_IS_REQUIRED = 
		"You must type a description to new task!";
	
	/** Mensagem para cria��o com sucesso de atividade */
	private static final String SUCESSO = 
		"A new task was add with success!";
	
	/**
	 * Construtor de FormAddTask
	 * @param midlet - Refer�ncia para TaskListMIDlet
	 */
	public FormAddTask(TaskListMidlet midlet) {
	
		super("Add Task");
		setLayout(new BoxLayout(BoxLayout.Y_AXIS));
		 
		this.midlet = midlet;
		
		// Cria o label e a descricao da tarefa para o Form de Add Task
		labelDescription = new Label("Task Description");
		textDescription = new TextField(40);
		
		Dimension dimension = new Dimension();
		dimension.setWidth(5);
		dimension.setHeight(30);
		
		labelHour = new Label("Time");
		
		textHour = new TextField(2);
		textHour.setPreferredSize( dimension);
		
		textMinute = new TextField(2);
		textMinute.setPreferredSize( dimension);
		
		// adiciona os componentes ao Form
		addComponent(labelDescription);
		addComponent(textDescription);
		
		addComponent(labelHour);
		addComponent(textHour);
		addComponent(textMinute);
		
		// cria array de radio buttons para prioridades e categorias
		radioPriority = createRadioButtonPriorityGroup();
		radioCategory = createRadioButtonCategoryGroup();
		
		labelCategory = new Label("Choose a category");
		addComponent(labelCategory);
		buttonGroupCategory = addListRadioButtonCategoryToForm();
		
		labelPriority = new Label("Choose a priority");
		addComponent(labelPriority);
		buttonGroupPriority = addListRadioButtonPriorityToForm();
		
		//calend�rio com a data corrente
		calendar = new Calendar();
		calendar.setDate(new Date());
		addComponent(calendar);
		
		save = new Command("Save", 0);
		addCommand(save);
		
		back = new Command("Back", 1);
		addCommand(back);
				
		addCommandListener(this);
		
		taskDAO = new TaskDAOImpl();
		
		setTransitionOutAnimator(in);
		setTransitionOutAnimator(out);
		
		show();
	}
	
	/**
	 * Agrupa os radio buttons de categoria em um ButtonGroup.
	 * @return ButtonGroup
	 */
	private ButtonGroup addListRadioButtonCategoryToForm() {
		
		buttonGroupCategory = new ButtonGroup();
		for (int tam = 0; tam < radioCategory.length; tam++) {
			addComponent(radioCategory[tam]);
			buttonGroupCategory.add(radioCategory[tam]);
		}
		return buttonGroupCategory;
	}
	
	/**
	 * Agrupa os radio buttons de prioridade em um ButtonGroup.
	 * @return ButtonGroup
	 */
	private ButtonGroup addListRadioButtonPriorityToForm() {
		
		buttonGroupPriority = new ButtonGroup();
		
		for (int tam = 0; tam < radioPriority.length; tam++) {
			addComponent(radioPriority[tam]);
			buttonGroupPriority.add(radioPriority[tam]);
		}
		return buttonGroupPriority;
	}
	
	/** 
	 * Cria��o da Lista de Prioridades para as tarefas
	 * @return RadioButton
	 */
	private RadioButton[] createRadioButtonPriorityGroup() {
		RadioButton[] radioButtonPriority = {new RadioButton(Priority.HIGH), new RadioButton(Priority.LOW)};
		radioButtonPriority[0].setSelected(true);
		return radioButtonPriority;
	}

	/** 
	 * Cria��o da Lista de Categorias para as tarefas
	 * @return RadioButton
	 */
	private RadioButton[] createRadioButtonCategoryGroup() {
		RadioButton[] radioButtonCategory = {
				new RadioButton(Category.WORK), new RadioButton(Category.LEISURE),
				new RadioButton(Category.APPOINTMENT), new RadioButton(Category.OTHER)};
		
		radioButtonCategory[0].setSelected(true);
		return radioButtonCategory;
	}

	/**
	 * Instancia objeto Task a partir de dados de entrada do usu�rio
	 * @return inst�ncia de Task
	 */
	public Task captureTaskForm() {
		
		Task task = new Task();
		
		String description = textDescription.getText().trim();
		Category category = new Category(buttonGroupCategory.getSelectedIndex());
		Priority priority = new Priority(buttonGroupPriority.getSelectedIndex());
		
		// Cria calendar com hora definida
		java.util.Calendar cal = java.util.Calendar.getInstance();
		cal.set( java.util.Calendar.HOUR, Integer.parseInt(textHour.getText()));
		cal.set( java.util.Calendar.MINUTE, Integer.parseInt(textMinute.getText()));
		cal.set(java.util.Calendar.AM_PM, 0);
		
		if(description.length() > 0) {
			task.setDescription(description);
		} else {
			Dialog.show("", DESCRIPTION_IS_REQUIRED, "OK", "");
			return null;
		}
		task.setCategory(category.getCode());
		task.setPriority(priority.getCode());
		task.setDateHour(cal.getTime().getTime());
		task.setCompleted(false);
		
		return task;
	}
	
	/** Tratamento de eventos de commands. */
	public void actionPerformed(ActionEvent ev) {
		
		Command cmd = ev.getCommand();
		
		if(cmd == null) return;
		
		switch(cmd.getId()){
		
			case 0:
				saveTask();
				break;
				
			case 1:
				back();
		}
	}
	
	/** Salva a nova tarefa */
	private void saveTask(){
		
		Task task = captureTaskForm();
		if(task != null) {
			taskDAO.saveTask(task);
			Dialog.show("", SUCESSO, "OK", "");
			back();
		}
	}
	
	/** Retorna para a tela de lista de tarefas */
	private void back(){
		this.midlet.setCurrentForm(new FormTaskList(this.midlet,  new PreferencesSearch()));
	}
}
