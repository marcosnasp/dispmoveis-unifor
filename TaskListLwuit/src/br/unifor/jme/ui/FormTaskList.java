package br.unifor.jme.ui;

import java.util.Vector;

import br.unifor.jme.midlet.TaskListMidlet;
import br.unifor.jme.model.Task;
import br.unifor.jme.rms.IRMSListener;
import br.unifor.jme.rms.PreferencesSearch;
import br.unifor.jme.rms.RMS;
import br.unifor.jme.rms.TaskDAOImpl;
import br.unifor.jme.sorter.TaskSorterByDataAsc;
import br.unifor.jme.util.Alarm;
import br.unifor.jme.util.TaskListUtils;

import com.sun.lwuit.ButtonGroup;
import com.sun.lwuit.Command;
import com.sun.lwuit.Dialog;
import com.sun.lwuit.Form;
import com.sun.lwuit.RadioButton;
import com.sun.lwuit.animations.CommonTransitions;
import com.sun.lwuit.animations.Transition;
import com.sun.lwuit.events.ActionEvent;
import com.sun.lwuit.events.ActionListener;
import com.sun.lwuit.layouts.BoxLayout;

/** Formul�rio que mostra a lista de tarefas */
public class FormTaskList extends Form implements ActionListener, IRMSListener {

	/** Command para inserir uma nova tarefa. */
	private Command cmdInsertTask;

	/** Command para mostrar detalhes de uma tarefa */
	private Command cmdDetailTask;

	/** Command para editar uma tarefa */
	private Command cmdUpdateTask;

	/** Command para excluir uma tarefa */
	private Command cmdDeleteTask;

	/** Command para registrar a realiza��o de uma tarefa */
	private Command cmdDoTask;

	/** Command para enviar tarefa via sms */
	private Command cmdSendSMS;

	/** Command para sair da aplica��o */
	private Command cmdExit;
	
	private Command cmdPreferences;

	/** Refer�ncia para TaskListMIDlet */
	private TaskListMidlet midlet;

	/** Vector que armazena a lista de tarefas */
	private Vector tasks;

	/** Vector de RadioButtons para exibir tarefas */
	private Vector vectorRadioButtons;
	
	/** Grupo de radio buttons de tarefas */
	private ButtonGroup buttonGroupTasks;

	/** DAO que encapsula as opera��es de persist�ncia. */
	private TaskDAOImpl taskDAO;
	
	/** Dura��o da transi��o entre telas */
	private static final int DURATION = 500;

	/** horizontal slide left to right */
	private Transition in = CommonTransitions.createSlide(
			CommonTransitions.SLIDE_HORIZONTAL, true, DURATION);

	/** horizontal slide right to left */
	private Transition out = CommonTransitions.createSlide(
			CommonTransitions.SLIDE_HORIZONTAL, true, DURATION);
	
	private static final String SELECTION_IS_REQUIRED = 
		"You must select a task to perform this operation!";
	
	private PreferencesSearch preferencesSearch;
	
	/**
	 * Construtor de FormTaskList
	 * @param midlet - Refer�ncia para TaskListMIDlet
	 */
	public FormTaskList(TaskListMidlet midlet) {

		super("TODO List");
		setLayout(new BoxLayout(BoxLayout.Y_AXIS));

		this.midlet = midlet;
		this.preferencesSearch = new PreferencesSearch();

		// operacoes no RMS
		taskDAO = new TaskDAOImpl();

		vectorRadioButtons = new Vector();
		buttonGroupTasks = new ButtonGroup();

		RMS.setListener(this);
		loadTasksRMSToVectorRadioButtons();
		
		//mostra todos os commands se houver tarefa cadastrada
		if(tasks.size() > 0){
			
			cmdDoTask = new Command("Mark as done", 0);
			cmdInsertTask = new Command("Add", 1);
			cmdDetailTask = new Command("Detail", 2);
			cmdUpdateTask = new Command("Edit", 3);
			cmdDeleteTask = new Command("Delete", 4);
			cmdSendSMS = new Command("Send by SMS", 5);
			cmdExit = new Command("Exit", 6);
			cmdPreferences = new Command("Preferences", 7);
			
			addCommand(cmdExit);
			addCommand(cmdPreferences);
			addCommand(cmdDeleteTask);
			addCommand(cmdUpdateTask);
			addCommand(cmdDetailTask);
			addCommand(cmdInsertTask);
			addCommand(cmdSendSMS);
			addCommand(cmdDoTask);
		}
		else{
			cmdInsertTask = new Command("Add", 1);
			cmdPreferences = new Command("Preferences", 7);
			cmdExit = new Command("Exit", 6);
			addCommand(cmdInsertTask);
			addCommand(cmdPreferences);
			addCommand(cmdExit);
		}

		addCommandListener(this);

		setTransitionOutAnimator(in);
		setTransitionOutAnimator(out);
		
		Alarm.registerNext(taskDAO.searchTasks(null, new TaskSorterByDataAsc()));
		
		show();
	}
	
	public FormTaskList(TaskListMidlet midlet, PreferencesSearch preferencesSearch) {
		
		super("TODO List");
		setLayout(new BoxLayout(BoxLayout.Y_AXIS));

		this.midlet = midlet;
		if(preferencesSearch != null) {
			this.preferencesSearch = preferencesSearch;
		} else {
			this.preferencesSearch = new PreferencesSearch();
		}
		
		// operacoes no RMS
		taskDAO = new TaskDAOImpl();
		
		vectorRadioButtons = new Vector();
		buttonGroupTasks = new ButtonGroup();

		RMS.setListener(this);
		loadTasksRMSToVectorRadioButtons();
		
		//mostra todos os commands se houver tarefa cadastrada
		if(tasks.size() > 0){
			
			cmdDoTask = new Command("Mark as done", 0);
			cmdInsertTask = new Command("Add", 1);
			cmdDetailTask = new Command("Detail", 2);
			cmdUpdateTask = new Command("Edit", 3);
			cmdDeleteTask = new Command("Delete", 4);
			cmdSendSMS = new Command("Send by SMS", 5);
			cmdExit = new Command("Exit", 6);
			cmdPreferences = new Command("Preferences", 7);
			
			addCommand(cmdExit);
			addCommand(cmdPreferences);
			addCommand(cmdDeleteTask);
			addCommand(cmdUpdateTask);
			addCommand(cmdDetailTask);
			addCommand(cmdInsertTask);
			addCommand(cmdSendSMS);
			addCommand(cmdDoTask);
		}
		else{
			cmdInsertTask = new Command("Add", 1);
			cmdExit = new Command("Exit", 6);
			addCommand(cmdInsertTask);
			addCommand(cmdExit);
		}

		addCommandListener(this);

		setTransitionOutAnimator(in);
		setTransitionOutAnimator(out);

		show();
		
	}

	/**
	 * Carrega lista de tarefas no Form f buscando no RMS e 
	 * atribuindo no vector de radio buttons.
	 * Formato da Task - Descricao - Data de conclus�o - Descri��o - 09/09/2010.
	 * @param f Form
	 */
	public void loadTasksRMSToVectorRadioButtons() {

		vectorRadioButtons.removeAllElements();
			
		// passando os filtros das preferencias do usuario e o comparador.
		tasks = taskDAO.searchTasks(preferencesSearch.getFilter(), preferencesSearch.getComparator());	
		
		Task task = null;
		StringBuffer sb = null;
		for (int i = 0; i < tasks.size(); i++) {

			sb = new StringBuffer();
			task = (Task) tasks.elementAt(i);
			sb.append(task.getDescription())
					.append(" - ")
					.append(TaskListUtils.getDataFormatada(task
							.getDateHour()));
			RadioButton radioButton = new RadioButton(sb.toString());
			vectorRadioButtons.addElement(radioButton);
			addComponent(radioButton);
			buttonGroupTasks.add(radioButton);
		}
	}

	/** Chama m�todo para carregar as tarefas. */
	public void registroPersistido() {
		this.loadTasksRMSToVectorRadioButtons();
	}

	/** Tratamento de eventos de commands. */
	public void actionPerformed(ActionEvent ev) {

		Command cmd = ev.getCommand();

		if (cmd == null) return;

		switch (cmd.getId()) {

			//marca tarefa como feita
			case 0:
				markTaskAsDone();
				break;
	
			// cria nova tarefa
			case 1:
				createNewTask();//OK
				break;
	
			// detalha tarefa selecionada
			case 2:
				detailTask();//OK
				break;
	
			// edita tarefa selecionada
			case 3:
				editTask();//ok
				break;
	
			// exclui tarefa selecionada
			case 4:
				deleteTask();//OK
				break;
	
			// envia tarefa selecionada por SMS
			case 5:
				sendBySMS();
				break;
	
			// sai da aplica��o
			case 6:
				exitApplication();//ok
				break;
	
			// seta prefer�ncias
			case 7:
				setPreferences();
				break;
				
		}
	}

	/**
	 * Retorna a tarefa atualmente selecionada na interface ou null caso n�o
	 * exista nenhuma tarefa marcada
	 * 
	 * @param alertIfNoneSelected
	 *            Indica se deve exibir um alerta caso nenhuma tarefa tenha sido
	 *            marcada na interface
	 * @return Task
	 * 
	 * */
	private Task getSelectedTask(boolean alertIfNoneSelected) {
		
		Task task = null;
		int index = buttonGroupTasks.getSelectedIndex();
		
		if(index >= 0){
			
			RadioButton radioButtonSelecionado = 
				buttonGroupTasks.getRadioButton(buttonGroupTasks.getSelectedIndex());
			
			String taskDescription = radioButtonSelecionado.getText();
			int offset = taskDescription.lastIndexOf('-');
			taskDescription = taskDescription.substring(0, --offset);
			task = taskDAO.searchOneTaskForDescription(taskDescription);
			
		} else if(alertIfNoneSelected){
		
			Dialog.show("", SELECTION_IS_REQUIRED, "OK", "");
		}
			
		return task;
	}

	/** Marca tarefa como lida e salva no RMS de tarefas conclu�das. */
	private void markTaskAsDone() {
		Task task = getSelectedTask(true);
		if(task != null){
			task.setCompleted(true);
			taskDAO.saveTask(task);	
			this.midlet.setCurrentForm(new FormTaskList(midlet, new PreferencesSearch()));
		}
	}

	/** Cria nova tarefa */
	private void createNewTask() {
		midlet.setCurrentForm(new FormAddTask(this.midlet));
	}

	/** Detalha informa��es da tarefa selecionada */
	private void detailTask() {
		Task task = getSelectedTask(true);
		if (task != null) {
			midlet.setCurrentForm(new FormTaskDetails(this.midlet, task));
		}
	}

	/** Edita tarefa selecionada */
	private void editTask() {
		Task task = getSelectedTask(true);
		if (task != null) {
			midlet.setCurrentForm(new FormEditTask(this.midlet, task));
		}
	}

	/** Exclui tarefa selecionada */
	private void deleteTask() {
	
		Task task = getSelectedTask(true);
		
		if(task != null){
			taskDAO.deleteTask(task);
			midlet.setCurrentForm(new FormTaskList(midlet));
		}
	}

	/** Envia tarefa selecionada por SMS */
	private void sendBySMS() {
		Task task = getSelectedTask(true);
		if (task != null) {
			midlet.setCurrentForm(new FormSendTaskSMS(this.midlet, task));
		}
	}

	/** Sai da aplica��o */
	private void exitApplication() {

		midlet.destroyApp(false);
		midlet.notifyDestroyed();
	}

	/** Altera prefer�ncias do usu�rio. */
	private void setPreferences() {
		midlet.setCurrentForm(new FormPreferences(this.midlet, this.preferencesSearch));
	}
	
}