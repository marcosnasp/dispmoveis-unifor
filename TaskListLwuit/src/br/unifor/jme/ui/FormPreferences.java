package br.unifor.jme.ui;

import javax.microedition.rms.RecordComparator;
import javax.microedition.rms.RecordFilter;

import br.unifor.jme.filter.FilterByCategory;
import br.unifor.jme.filter.FilterByCompletedTasks;
import br.unifor.jme.filter.FilterByPriority;
import br.unifor.jme.filter.FilterByUncompletedTasks;
import br.unifor.jme.midlet.TaskListMidlet;
import br.unifor.jme.model.Category;
import br.unifor.jme.model.Priority;
import br.unifor.jme.rms.PreferencesSearch;
import br.unifor.jme.sorter.TaskSorterByDataAsc;
import br.unifor.jme.sorter.TaskSorterByDataDesc;

import com.sun.lwuit.ComboBox;
import com.sun.lwuit.Command;
import com.sun.lwuit.Form;
import com.sun.lwuit.Label;
import com.sun.lwuit.events.ActionEvent;
import com.sun.lwuit.events.ActionListener;
import com.sun.lwuit.layouts.BoxLayout;

/**
 * Form para configura��o da apresenta��o das tarefas.
 * A forma como as tarefas dever�o ser exibidas.
 * Tipos: Ordenadas por data: Ascendente, Descendente.
 * Filtros: Filtro por prioridade, por categoria, exibir todas as tarefas.
 *
 */
public class FormPreferences extends Form implements ActionListener{

	private Label order;
	
	private Label filter;
	
	private ComboBox typeDate;
	
	private ComboBox typeFilter;
	
	private Command cmdConfirm;
	
	private Command cmdBack;
	
	private TaskListMidlet midlet;
	
	private PreferencesSearch preferencesSearch;
	
	public FormPreferences(TaskListMidlet midlet, PreferencesSearch preferencesSearch) {
	
		super("Preferences");
		setLayout(new BoxLayout(BoxLayout.Y_AXIS));
		this.midlet = midlet;
		this.preferencesSearch = preferencesSearch;
		
		order = new Label("Order by date");
		filter = new Label("Filter by");
		
		typeDate = buildComboBoxOrderByDate();
		typeFilter = buildComboBoxFilter();
		
		addComponent(order);
		addComponent(typeDate);
		
		addComponent(filter);
		addComponent(typeFilter);
		
		cmdConfirm = new Command("Confirm", 0);
		cmdBack = new Command("Back", 1);
		
		addCommand(cmdConfirm);
		addCommand(cmdBack);
		
		addCommandListener(this);
		
		show();
	}
	
	private ComboBox buildComboBoxOrderByDate(){
		
		ComboBox combo = new ComboBox();
		combo.addItem("ascending");
		combo.addItem("descending");
		
		return combo;
	}
	
	private ComboBox buildComboBoxFilter(){
		
		ComboBox combo = new ComboBox();
		combo.addItem("Category: " + Category.APPOINTMENT);
		combo.addItem("Category: " + Category.LEISURE);
		combo.addItem("Category: " + Category.OTHER);
		combo.addItem("Category: " + Category.WORK);
		
		combo.addItem("Priority: " + Priority.HIGH);
		combo.addItem("Priority: " + Priority.LOW);
		
		combo.addItem("Show all completed tasks");
		combo.addItem("Show all uncompleted tasks");
		
		combo.addItem("Show all tasks");

		return combo;
	}

	public void actionPerformed(ActionEvent ev) {
		
		if(ev.getSource() == cmdConfirm) {
			
			RecordFilter filter = captureFilterToForm();
			RecordComparator comparator = captureComparatorToForm();
			
			preferencesSearch = new PreferencesSearch(filter, comparator);
			
			this.midlet.setCurrentForm(new FormTaskList(this.midlet, preferencesSearch));
		} else if(ev.getSource() == cmdBack) {
			this.midlet.setCurrentForm(new FormTaskList(midlet, new PreferencesSearch()));
		}
	}
	
	/** Obt�m comparador escolhido na interface */
	private RecordComparator captureComparatorToForm() {

		RecordComparator comparator = null;
		
		switch (typeDate.getSelectedIndex()) {
			case 0:
				comparator =  new TaskSorterByDataAsc();
				break;
			
			case 1:
				comparator = new TaskSorterByDataDesc();
				break;
				
			default:
				break;
		}
		return comparator;
		
	}

	/** Obt�m filtro escolhido na interface */
	private RecordFilter captureFilterToForm() { 
		
		RecordFilter filter = null;
		
		switch (typeFilter.getSelectedIndex()) {
			case 0:
				filter =  new FilterByCategory(new Category(Category.APPOINTMENT));
				break;
			
			case 1:
				filter = new FilterByCategory(new Category(Category.LEISURE));
				break;
				
			case 2:
				filter = new FilterByCategory(new Category(Category.OTHER));
				break;
				
			case 3:
				filter = new FilterByCategory(new Category(Category.WORK));
				break;
				
			case 4:
				filter = new FilterByPriority(new Priority(Priority.HIGH));
				break;
				
			case 5:
				filter = new FilterByPriority(new Priority(Priority.LOW));
				break;
				
			case 6:
				filter = new FilterByCompletedTasks();
				break; 
				
			case 7: 
				filter = new FilterByUncompletedTasks();
				break;
				
			case 8: 
				filter = null;
				break;
				
			default:
				break;
		}
		return filter;
		
	}

	/**
	 * Configura as preferencias de pesquisa e ordenacao
	 * para as tarefas. 
	 * @param preferencesSearch
	 */
	public void setPreferencesSearch(PreferencesSearch preferencesSearch) {
		this.preferencesSearch = preferencesSearch;
	}

	public PreferencesSearch getPreferencesSearch() {
		return preferencesSearch;
	}
}
