package br.unifor.jme.ui;

import br.unifor.jme.midlet.TaskListMidlet;
import br.unifor.jme.model.Task;
import br.unifor.jme.net.SMSSender;
import br.unifor.jme.rms.IRMSListener;
import br.unifor.jme.rms.RMS;

import com.sun.lwuit.Command;
import com.sun.lwuit.Form;
import com.sun.lwuit.Label;
import com.sun.lwuit.TextField;
import com.sun.lwuit.animations.CommonTransitions;
import com.sun.lwuit.animations.Transition;
import com.sun.lwuit.events.ActionEvent;
import com.sun.lwuit.events.ActionListener;
import com.sun.lwuit.layouts.BoxLayout;

public class FormSendTaskSMS extends Form implements ActionListener, IRMSListener {

	private Command cmdSendSms;
	private Command cmdBack;
	
	private TextField smsDestination;
	
	private Label labelSmsDestination;
	private Label labelDescription;
	
	private Task task;
	private TaskListMidlet midlet;
	
	/** Dura��o da transi��o entre telas */
	private static final int DURATION = 500;

	/** horizontal slide left to right */
	private Transition in = CommonTransitions.createSlide(
			CommonTransitions.SLIDE_HORIZONTAL, true, DURATION);

	/** horizontal slide right to left */
	private Transition out = CommonTransitions.createSlide(
			CommonTransitions.SLIDE_HORIZONTAL, true, DURATION);
	
	public FormSendTaskSMS(TaskListMidlet midlet, Task task) {
		
		super("Tasks List");
		setLayout(new BoxLayout(BoxLayout.Y_AXIS));
		
		this.task = task;
		this.midlet = midlet;
		
		RMS.setListener(this);
		
		smsDestination = new TextField(20);
		
		labelDescription = new Label("Task description: " + task.getDescription());
		labelSmsDestination = new Label("Send task by SMS to");
		
		cmdSendSms = new Command("Send");
		cmdBack = new Command("Back");
		
		// adiciona os componentes ao Form
		addComponent(labelSmsDestination);
		addComponent(smsDestination);
		
		addComponent(labelDescription);
		
		addCommand(cmdSendSms);
		addCommand(cmdBack);
		
		addCommandListener(this);
		
		setTransitionOutAnimator(in);
		setTransitionOutAnimator(out);
		
		show();
	}
	
	public void registroPersistido() {
		
	}

	public void actionPerformed(ActionEvent ev) {
		// Envia SMS
		SMSSender smsSender = new SMSSender();
		if( ev.getSource() == cmdSendSms) {
			smsSender.send(task, smsDestination.getText());
			// Volta para tela principal
			this.midlet.setCurrentForm(new FormTaskList(this.midlet));
		} else if ( ev.getSource() == cmdBack) {
			this.midlet.setCurrentForm(new FormTaskList(this.midlet));
		} 
	}
}
